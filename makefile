libs := -levent_core -levent -levent_pthreads -levent_extra -lpthread  
cflags := -O2 -static  -Wall
CC = gcc

LDDIR += -L/opt/libevent/lib

INCLUDEDIR += -I./ \
			  -I../ \
			  -I/opt/libevent/include

headers := $(wildcard *.h)

all:
	make Client
	make Server
	chmod a+x Server
	chmod a+x Client
Client:client.c $(headers)
	$(CC) $< $(INCLUDEDIR) $(cflags) $(libs) $(LDDIR) -v -o $@
Server:server.c $(headers)
	$(CC) $< $(INCLUDEDIR) $(cflags) $(libs) $(LDDIR) -v -o $@

.PHONY : clean
clean:
	rm Client Server
